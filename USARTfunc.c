
// These are not C statements but directives to the C18 compiler. They result in configuration
// bits in the PIC18F4525 being set in the flash memory. This configuration is set when the 
// PIC is programmed by the PICkit2 programmer and before any program starts to run in the PIC.
 
#pragma config WDT = OFF
#pragma config OSC = INTIO7      // puts osc/4 on pin 10 (0n 28 pin package) to check freq
#pragma config MCLRE = OFF
#pragma config LVP = OFF
#pragma config PBADEN = OFF      // PORTB<4:0> are digital IO 

//****************************************************************************************
#include <stdio.h>
#include <p18f2620.h>
#include <usart.h>       // library containing serial communtication functions
#include <delays.h>      // library containing delays - i.e. wait for time t


void resetRxEnable(void);

static unsigned char buf_st = 0;
static unsigned char buf_i = 0;
char sdt_out_buf[120];

void configureUSART(unsigned long baudrate, unsigned char osc_freq_MHz)
{
  unsigned int spbrg;
 
  TRISCbits.TRISC6 = 0;     // set TX (RC6) as output 
  TRISCbits.TRISC7 = 1;     // and RX (RC7) as input
 
  // For a 16-bit sbprg value with USART_BRIGH_HIGH setting.
  // Formula from datasheet is Baudrate = FOSC/ (4 * (spbrg + 1 ))
  spbrg = (unsigned int)( ((float)osc_freq_MHz * 1.0e6) /(4.0*(float)baudrate) + 0.5 - 1.0); 
 
 
  OpenUSART( USART_TX_INT_OFF & USART_RX_INT_OFF & USART_ASYNCH_MODE & USART_EIGHT_BIT & 
             USART_CONT_RX & USART_BRGH_HIGH, spbrg );  
  // OpenUSART is part of the C18 usart.h library        
 
  BAUDCONbits.BRG16 = 1;  	// needed so we can use a 16-bit spbrg
				// Note that this is not discussed in the c18 Compiler Libraries guide
  Delay10KTCYx(1); // small 4x0.0125 s delay to allow communication speed to stabilize
					// part of the C18 delays.h library
}                      // original delay was 0.0125 s but that was with Fosc at 32 MHz (now is 8MHz)

void test_overrun(void)
{
  if(RCSTAbits.OERR == 1u) resetRxEnable(); //check for overrunn error; reset if needed
}

void resetRxEnable(void)    
{ 
    printf("**");
    RCSTAbits.CREN = 0;
    RCSTAbits.CREN = 1;
}


unsigned char echoRx(void)
{
    unsigned char rx=0;

    if(DataRdyUSART())  
    {
        rx = ReadUSART();
        printf("%c",rx); // changed from WriteUSART(rx) to 
                         //    1) ensure nothing is overwritten in the UART and
                         //    2) ensure this character does not land in the middle
                         //       of a longer message. stdout now points at a buffer so
                         //       printf() adds to the end of the buffer.
    }
    return rx;
}


//***********************************************************************************
//                          set_osc_8MHz()
//    sets the oscillator from the default 1 MHz to 8 MHz
//***********************************************************************************
 
void set_osc_8MHz(void)
{
  int i;
 
  OSCCONbits.IRCF2 = 1;     // Set the OSCILLATOR Control Register to 8 MHz
  OSCCONbits.IRCF1 = 1;      
  OSCCONbits.IRCF0 = 1;        
}

void WaitOneSecond(void)
{
    Delay10KTCYx(200); // wait 10 000 * 200 * TCY = 1.000 seconds
 
}

// stdout has been changed to _H_USER which means the character handing
//    functing now called by printf() is _user_putc(char c). This function
//    puts characters in a buffer. Another function process_outBuffer() will
//    be called from the event while loop in main() and it will send characters
//    from the buffer to the UART when space is available.
int _user_putc (char c)
{

    if(buf_i<118u)
    {
        sdt_out_buf[buf_i] = c;
        buf_i++;
        return 0; 
    } 
    else return EOF; 
}

// process_outBuffer is called from the event while loop in main(). It will send
// a character from the output buffer if available and if the UART is not already
// full.
void process_outBuffer(void)
{
    if(!BusyUSART()&& (buf_i > 0u) )
    {
        if(buf_st < buf_i)
        {
           putcUSART(sdt_out_buf[buf_st]);
           buf_st++;
        }
        if(buf_st == buf_i)
        {
           buf_st = 0;
           buf_i = 0;
           sdt_out_buf[0] = \0;
        } 
        Nop(); 
    }
}


