#include <p18f2620.h>
#include <usart.h>
#include <stdio.h>
#include "commands.h"

void open_led(void)
{
    TRISCbits.TRISC4 = 0; // LED is attached to RC4 make output
                          // pin15
}
void process_key(unsigned char Rx)
{

     switch (Rx)
     {
        case 'L':
        case 'l':
           PORTCbits.RC4 = 1;   //  LED On
           break;
        case 'F':
        case 'f':
           PORTCbits.RC4 = 0;   //  LED OFF
           break;
        case 'M':
        case 'm':
           start_menu();
           break; 
        case '\r':
           printf("\n");
           break; 

     }

}

void start_menu(void)
{
    printf("\tpic18lf2620\r\nAugust 19, 2012\r\n");
    printf("by Dan Peirce B.Sc.\r\nEcho to sender & LED....\r\n");
    printf("L - LED on \r\nF - LED off\r\n\r\n");
}

