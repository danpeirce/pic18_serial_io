//***************************************************************************************
 
#include <stdio.h>         
#include <delays.h>
#include "USARTfunc.h"
#include "commands.h"
 
void main(void)  
{
    unsigned char Rx=0;

    set_osc_8MHz(); // set MCU to run at 8 MHz
    configureUSART(115200ul, 8);
    open_led();
    stdout = _H_USER;               //to make printf() non blocking
                                    // related to functions defined in USARTfunc.c
                                    // _user_putc (char c) and process_outBuffer().
                                    // _H_USER defined in stdio.h
    start_menu();

    test_overrun();

    while(1)
    {
       
       Rx = echoRx();      // echos to the out buffer

       if (Rx)  process_key(Rx);
       process_outBuffer();  // defined in USARTfunc.c This function
                             // sends characters from the buffer to the UART
                             // if the UART has space available and there is one
                             // or more characters waiting in the buffer
       test_overrun();         
    }	
 
}





 

