# PIC18 Serial Respond to Raspberry Pi
The PIC18LF2620 programmed to repond to commands from the [Raspberry Pi](http://danpeirce.net46.net/dokuwiki/doku.php?id=raspberry_pi)  (or any system) over a serial connection. This can be either ttyAMA0 or ttyUSB0. 

## Target MCU
A PIC18LF2620 is the target MCU.
The project file was created using version 8.5 MPlab IDE (free from Microchip).
The project can be migrated to the newer MPlab IDE X.

## MPlab IDE does not support git
There is no support in MPlab for git. With this in mind I simply close MPlab IDE if/when changing branches. One can open the Project in MPlab IDE by a double mouse click (left button) on the project file (*.mcp).

## Webpage
[Link to starting point for project](http://danpeirce.net46.net/dokuwiki/doku.php?id=rpi_pic_led)